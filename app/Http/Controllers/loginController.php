<?php

namespace App\Http\Controllers;

use App\Mail\resetMail;
use App\Mail\verificationEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
class loginController extends Controller
{
    //


    public function create_account(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'fullname'=>'required',
            'phone'=>'required',
            'password'=>'required'
        ]);

        $details=$request->input();


        $User=new User();
        $User->fullname=$details['fullname'];
        $User->email=$details['email'];
        $User->phone=$details['phone'];
        $User->status=1;
        $User->password=bcrypt($details['password']);
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();


        $link=env('APP_URL_FRONTEND')."/verify-account/".$User->remember_token;
        Mail::to($details['email'])->sendNow(new verificationEmail([
            'link'=>$link
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Your registration was successfull"
        ]);

    }

    public function verify_account(Request $request){
        $token=$request->query('code');

        if(!isset($token)){
            return response()->json([
                'status'=>false,
                'msg'=>"Token not found"
            ]);
        }


        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->email_verified_at=date('Y-m-d h:i:s');
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json([
            'status'=>true,
            'msg'=>"Account verified"
        ]);
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->first();
            return response()->json([
                'status'=>true,
                'msg'=>"Your registration was successfull",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
    }

    public function forget_password(Request $request){
        $this->validate($request,[
            'email'=>'required',
        ]);

        $User=User::where('email',$request->input('email'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->remember_token=mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12);
        $User->save();

        $link=env('APP_URL_FRONTEND')."/reset-password/".$User->remember_token;

        Mail::to($User->email)->sendNow(new resetMail([
            'link'=> $link
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token=$request->query('code');

        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"correct token" ]);
    }

    public function reset_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
            'token'=>'required'
        ]);

        $User=User::where('remember_token',$request->input('token'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->password=bcrypt($request->input('password'));
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json(['status'=>true,'msg'=>"Password reset was successful" ]);
    }
}
