<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/v1/')->group(function(){
    Route::post('login',"loginController@login");
    Route::post('create-account',"loginController@create_account");
    Route::get('verify-account',"loginController@verify_account");

    Route::post('forget-password',"loginController@forget_password");
    Route::post('reset-password',"loginController@reset_password");
    Route::get('verify-reset-token',"loginController@verify_password");

});
